@extends('layouts.admin')


{{-- modal add item --}}
<div class="modal fade" id="modalAddItem" tabindex="-1" aria-labelledby="modalAddItem" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalAddItem">Add New Items</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('add-item.store') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="item_name">Item Name</label>
                        <input type="text" id="item_name" name="item_name" value="{{ old('') }}" placeholder="Insert Item Name" class="form-control">
                        {{-- @error('nama_barang') <code>{{ $message }}</code> @enderror --}}
                    </div>
                    <div class="form-group">
                        <label for="specification">Specification</label>
                        <input type="text" id="specification" name="specification" value="{{ old('') }}" placeholder="Insert Specification" class="form-control">
                        {{-- @error('harga') <code>{{ $message }}</code> @enderror --}}
                    </div>
                    <div class="form-group">
                        <label for="condition">Condition</label>
                        <select name="condition" id="condition" class="form-control">
                            <option selected>-- Select --</option>
                            <option value="Good">Good</option>
                            <option value="Repair">Repair</option>
                            <option value="Damaged">Damaged</option>
                            <option value="Need Repair">Need Repair</option>
                            <option value="Slightly Broken">Slightly Broken</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="location">Location</label>
                        <input type="text" id="location" name="location" value="{{ old('') }}" placeholder="Insert Location" class="form-control">
                        {{-- @error('harga') <code>{{ $message }}</code> @enderror --}}
                    </div>
                    <div class="form-group">
                        <label for="item_qty">Item Qty</label>
                        <input type="number" id="item_qty" name="item_qty" value="{{ old('') }}" placeholder="Insert Item Qty" class="form-control">
                        {{-- @error('harga') <code>{{ $message }}</code> @enderror --}}
                    </div>
                    <div class="form-group">
                        <label for="item_source">Item Source</label>
                        <input type="text" id="item_source" name="item_source" value="{{ old('') }}" placeholder="Insert Item Source" class="form-control">
                        {{-- @error('harga') <code>{{ $message }}</code> @enderror --}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Add</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- modal edit item --}}

<!-- Modal edit user -->
<div class="modal fade" id="modalEditItem" tabindex="-1" aria-labelledby="modalEditItem" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalEditItem">Edit User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" class="formEditItem">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <div class="form-group">
                        <label for="item_name">Nama Barang</label>
                        <input type="text" id="item_name" name="item_name" placeholder="Nama Barang" class="form-control edititem_name" required>
                    </div>
                    <div class="form-group">
                        <label for="specification">Specification</label>
                        <input type="text" id="specification" name="specification" placeholder="Nama Barang" class="form-control editspecification" required>
                    </div>
                    <div class="form-group">
                        <label for="location">Location</label>
                        <input type="text" id="location" name="location" placeholder="Nama Barang" class="form-control editlocation" required>
                    </div>
                    <div class="form-group">
                        <label for="condition">Condition</label>
                        <input type="text" id="condition" name="condition" placeholder="Nama Barang" class="form-control editcondition" required>
                    </div>
                    <div class="form-group">
                        <label for="item_qty">Item Qty</label>
                        <input type="number" id="item_qty" name="item_qty" placeholder="Nama Barang" class="form-control edititem_qty" required>
                    </div>
                    <div class="form-group">
                        <label for="item_source">Item Source</label>
                        <input type="text" id="item_source" name="item_source" placeholder="Nama Barang" class="form-control edititem_source" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-user-edit"></i></i> Edit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@section('main-content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col"><h4>Item List</h4></div>
            <div class="col text-right">
                <button type="button" href="#!" class="btn btn-primary" data-toggle="modal" data-target="#modalAddItem"><i class="fas fa-plus-circle"></i> Add Items</button>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table id="example" class="table table-striped table-responsive-md" style="width:100%">
            <thead>
                <tr class="text-center">
                    <th>#</th>
                    <th>Item Name</th>
                    <th>Specification</th>
                    <th>Location</th>
                    <th>Condition</th>
                    <th>Qty</th>
                    <th>Source</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($add_item as $item)
                    <tr>
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td>{{ $item->item_name }}</td>
                        <td>{{ $item->specification }}</td>
                        <td>{{ $item->location }}</td>
                        <td>{{ $item->condition }}</td>
                        <td>{{ $item->item_qty }}</td>
                        <td>{{ $item->item_source }}</td>
                        <td class="d-flex text-center">
                            <button class="btn btn-warning mr-1" data-id="{{ $item->id }}" onclick="triggerEdit({{ $item->id }})"><i class="fas fa-user-edit mr-1"></i></a></button>
                            <a class="btn btn-danger ml-1" href="{{ url('/delete-item', $item->id) }}" role="button"><i class="fas fa-trash"></i></a>
                            {{-- <button class="btn btn-warning btn-block mr-2 align-items-center" ><i class="fas fa-user-edit mr-1"></i></a>
                            <button class="btn btn-warning btn-block mr-2 align-items-center" ><i class="fas fa-user-edit mr-1"></i></a>
                            <button class="btn btn-danger btn-block align-items-center" ><i class="fas fa-trash-alt mr-1"></i></button> --}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready( function () {
        $('#example').DataTable();
    } );


    function triggerEdit(id) {
        console.log("Hai")
       $.ajax({
           url: "{{ route('add-item.show','') }}/"+id,
           type: "GET",
           success: function(res){
               console.log(res)

               var elForm = document.querySelector("#modalEditItem form")
               elForm.setAttribute("action", "{{ url('add-item','') }}/"+id)
               $("#modalEditItem").modal('show');
                console.log(res.project);
               $("#modalEditItem .edititem_name").val(res.item_name);
               $("#modalEditItem .editlocation").val(res.location);
               $("#modalEditItem .editcondition").val(res.condition);
               $("#modalEditItem .edititem_qty").val(res.item_qty);
               $("#modalEditItem .editspecification").val(res.specification);
               $("#modalEditItem .edititem_source").val(res.item_source);
           },
           error: function(err){
                console.log(err)
           }
       })
    }

    $(document).ready(function() {
        console.log('tes')
        $('#triggerEdit').click(function(e) {
            console.log('s');
            let id = $(this).attr('data-id');
            console.log('a');
            console.log("{{ route('add-item.show','') }}/" + id)
            $.get({
                url: "{{ route('add-item.show','') }}/" + id,
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                },
                success: function(res) {
                    console.log('sukses nih');
                    console.log(res);

                    $(".edititem_name").val(res.item_name);
                    $(".editspecification").val(res.specification);
                    $(".editlocation").val(res.location);
                    $(".editcondition").val(res.condition);
                    $(".edititem_qty").val(res.item_qty);
                    $(".edititem_source").val(res.item_source);
                },
                error: function(err) {
                    console.log('gagal');
                    console.log(err)
                }
            })
        });
    });

</script>
@endsection
