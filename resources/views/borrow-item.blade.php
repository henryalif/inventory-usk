@extends('layouts.admin')

<div class="modal fade" id="modalAddItem" tabindex="-1" aria-labelledby="modalAddItem" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalAddItem">Borrow Items</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('borrow-item.store') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="borrower">Insert Borrower Name</label>
                        <input type="text" id="borrower" name="borrower" value="{{ Auth::user()->name }}" value="{{ old('') }}" placeholder="Insert Insert Borrower Name" class="form-control" readonly>
                        {{-- @error('harga') <code>{{ $message }}</code> @enderror --}}
                    </div>
                    <div class="form-group">
                        <label for="user_id">Insert Borrower Name</label>
                        <input type="text" id="user_id" name="user_id" value="{{ Auth::user()->id }}" value="{{ old('') }}" placeholder="Insert Insert Borrower Name" class="form-control" readonly>
                        {{-- @error('harga') <code>{{ $message }}</code> @enderror --}}
                    </div>
                    <div class="form-group">
                        <label for="">Borrow Date</label>
                        <input type="date" class="form-control" name="borrow_date" id="borrow_date" value="{{ old('') }}" placeholder="Insert Borrow Date">
                    </div>
                    <div class="form-group">
                        <label for="">Items</label>
                        <select name="item_name" id="item_name" value="{{ old('') }}" class="form-control editItem">
                            <option selected>-- Select --</option>
                            @foreach($get_item as $item)
                            <option value="{{ $item->item_name }}">{{ $item->item_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group" type="hidden">
                        <label for="">Items</label>
                        <select name="item_id" id="item_id" value="{{ old('') }}" class="form-control editItem">
                            <option selected>-- Select --</option>
                            @foreach($get_item as $item)
                            <option value="{{ $item->id }}">{{ $item->id }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Condition</label>
                        <select name="condition" id="condition" value="{{ old('') }}" class="form-control editCondition">
                            <option selected>-- Select --</option>
                            @foreach($get_item as $item)
                            <option value="{{ $item->condition }}">{{ $item->condition }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="item_qty">Item Qty</label>
                        <input type="number" id="item_qty" name="item_qty" value="" value="{{ old('') }}" placeholder="Insert Item Qty" class="form-control">
                        {{-- @error('harga') <code>{{ $message }}</code> @enderror --}}
                    </div>
                    <div class="form-group">
                        <label for="">Return Date</label>
                        <input type="date" class="form-control" name="return_date" id="return_date" value="{{ old('') }}" placeholder="Insert Return Date">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Add</button>
                </div>
            </form>
        </div>
    </div>
</div>

@section('main-content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col"><h4>Item List</h4></div>
            <div class="col text-right">
                <button type="button" href="#!" class="btn btn-primary" data-toggle="modal" data-target="#modalAddItem"><i class="fas fa-plus-circle"></i> Borrow Items</button>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table id="example" class="table table-striped" style="width:100%">
            <thead>
                <tr class="text-center">
                    <th>#</th>
                    <th>Borrower</th>
                    <th>Borrow Date</th>
                    <th>Item Name</th>
                    <th>Qty</th>
                    <th>Condition</th>
                    <th>Return Date</th>
                    <th>Action</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($view_borrow as $item)
                <tr class="text-center">
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->borrower }}</td>
                        <td>{{ date('d-m-Y', strtotime($item->borrow_date)); }}</td>
                        <td>{{ $item->item_borrowed }}</td>
                        <td>{{ $item->item_qty }}</td>
                        <td>{{ $item->condition }}</td>
                        <td>{{ date('d-m-Y', strtotime($item->return_date)); }}</td>
                        <td class="text-center">
                            <a class="btn btn-danger ml-1" href="{{ url('/borrow-delete', $item->id) }}" role="button"><i class="fas fa-trash"></i></a>
                            {{-- <button class="btn btn-warning btn-block mr-2 align-items-center" ><i class="fas fa-user-edit mr-1"></i></a>
                            <button class="btn btn-warning btn-block mr-2 align-items-center" ><i class="fas fa-user-edit mr-1"></i></a>
                            <button class="btn btn-danger btn-block align-items-center" ><i class="fas fa-trash-alt mr-1"></i></button> --}}
                        </td>
                        <td></td>
                    </tr>
                    @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready( function () {
        $('#example').DataTable();
    } );
</script>
@endsection
