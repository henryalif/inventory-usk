@extends('layouts.admin')

@section('main-content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col"><h4>Loan Data List</h4></div>
            <div class="col text-right">
                <a class="btn btn-success" href="{{ url('/loan-export') }}" role="button"><i class="fas fa-print"></i>  Print </a>
                {{-- <button type="button" href="#!" class="btn btn-primary" data-toggle="modal" data-target="#modalTambahDriver"><i class="fas fa-plus-circle"></i> Add Items</button> --}}
            </div>
        </div>
    </div>
    <div class="card-body">
        <table id="example" class="table table-striped" style="width:100%">
            <thead>
                <tr class="text-center">
                    <th>#</th>
                    <th>Borrower</th>
                    <th>Borrow Date</th>
                    <th>Item Name</th>
                    <th>Qty</th>
                    <th>Condition</th>
                    <th>Return Date</th>
                    <th>Action</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($view_borrow as $item)
                <tr class="text-center">
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->borrower }}</td>
                        <td>{{ date('d-m-Y', strtotime($item->borrow_date)); }}</td>
                        <td>{{ $item->item_borrowed }}</td>
                        <td>{{ $item->item_qty }}</td>
                        <td>{{ $item->condition }}</td>
                        <td>{{ date('d-m-Y', strtotime($item->return_date)); }}</td>
                        <td class="text-center">
                            <a class="btn btn-danger ml-1" href="{{ url('/borrow-delete', $item->id) }}" role="button"><i class="fas fa-trash"></i></a>
                            {{-- <button class="btn btn-warning btn-block mr-2 align-items-center" ><i class="fas fa-user-edit mr-1"></i></a>
                            <button class="btn btn-warning btn-block mr-2 align-items-center" ><i class="fas fa-user-edit mr-1"></i></a>
                            <button class="btn btn-danger btn-block align-items-center" ><i class="fas fa-trash-alt mr-1"></i></button> --}}
                        </td>
                        <td></td>
                    </tr>
                    @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready( function () {
        $('#example').DataTable();
    } );
</script>
@endsection
