<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $add_item = Item::all();
        return view('add-item', compact('add_item'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Item::create([
            'item_name' => $request->item_name,
            'specification' => $request->specification,
            'location' => $request->location,
            'condition' => $request->condition,
            'item_qty' => $request->item_qty,
            'location' => $request->location,
            'item_source' => $request->item_source
        ]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $itembyId = Item::find($id);
        return response()->json($itembyId);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_item = Item::findorfail($id);
        return view('add-item', compact('edit_item'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $itemUpdate = Item::find($id);
        $itemUpdate->item_name = $request->item_name;
        $itemUpdate->specification = $request->specification;
        $itemUpdate->location = $request->location;
        $itemUpdate->condition = $request->condition;
        $itemUpdate->item_qty = $request->item_qty;
        $itemUpdate->item_source = $request->item_source;

        $itemUpdate->save();
        return redirect()->route('add-item');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Item::destroy($id);
        return back();
    }
}
