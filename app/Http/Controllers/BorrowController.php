<?php

namespace App\Http\Controllers;

use App\Borrows;
use Illuminate\Http\Request;
use App\Http\Controllers\ItemController;
use App\Item;
use Illuminate\Support\Facades\DB;

class BorrowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list_item = [];
        $get_item = Item::all();
        $view_borrow = Borrows::all();
        // dd($get_item);
        return view('borrow-item', compact('view_borrow', 'get_item', 'list_item'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Borrows::create([
            'user_id'       => $request->user_id,
            'item_id'       => $request->item_id,
            'borrower'      => $request->borrower,
            'borrow_date'   => $request->borrow_date,
            'item_borrowed' => $request->item_name,
            'item_qty'      => $request->item_qty,
            'condition'     => $request->condition,
            'return_date'   => $request->return_date,
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $borrowId = Borrows::find($id);
        return response()->json($borrowId);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_borrow = Borrows::findorfail($id);
        return view('borrow-item', compact('edit_borrow'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $borrowUpdate = Borrows::find($id);
        $borrowUpdate->user_id          = $request->user_id;
        $borrowUpdate->item_id          = $request->item_id;
        $borrowUpdate->borrower         = $request->borrower;
        $borrowUpdate->borrow_date      = $request->borrow_date;
        $borrowUpdate->item_borrowed    = $request->item_name;
        $borrowUpdate->item_qty         = $request->item_qty;
        $borrowUpdate->condition        = $request->condition;
        $borrowUpdate->return_date      = $request->return_date;

        $borrowUpdate->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Borrows::destroy($id);
        return back();
    }

}

