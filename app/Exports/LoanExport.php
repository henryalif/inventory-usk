<?php

namespace App\Exports;

use App\Borrows;
use Maatwebsite\Excel\Concerns\FromCollection;

class LoanExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Borrows::all();
    }
}
