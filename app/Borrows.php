<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Borrows extends Model
{
    use HasFactory;
    protected $table = "borrows";
    protected $fillable = [
        'user_id',
        'item_id',
        'borrower', //user yg pinjam
        'borrow_date',
        'item_borrowed',
        'item_qty',
        'condition',
        'return_date'
    ];

    public function borrowItem(){
        return $this->hasMany(Item::class);
    }
}
