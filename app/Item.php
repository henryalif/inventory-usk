<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;
    protected $table = "items";
    protected $fillable = [
        'item_name',
        'specification',
        'location',
        'condition',
        'item_qty',
        'item_source'
    ];

    public function itemToBorrowers(){
        return $this->belongsTo(Borrows::class);
    }
}
