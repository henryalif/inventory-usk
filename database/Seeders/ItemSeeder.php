<?php

namespace Database\Seeders;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Seeder;
use App\Item;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Item::create(
            [
                'item_name' => 'Chair',
                'specification' => 'Blue',
                'location' => 'Gudang',
                'condition' => 'Good',
                'item_qty' => '90',
                'item_source' => 'Dana Bos',
            ]
        );
        Item::create(
            [
                'item_name' => 'Meja',
                'specification' => 'Blue',
                'location' => 'Kelas',
                'condition' => 'Repair',
                'item_qty' => '9',
                'item_source' => 'Dana Bos',
            ]
        );
        Item::create(
            [
                'item_name' => 'Computer',
                'specification' => 'Lenovo',
                'location' => 'Lab RPL',
                'condition' => 'Broken',
                'item_qty' => '40',
                'item_source' => 'Dana Bos',
            ]
        );
    }
}
