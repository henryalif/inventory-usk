<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'name' => 'Administrator',
                'last_name' => 'Administrator',
                'email' => 'test@gmail.com',
                'password' => Hash::make("test@gmail.com"),
                'level' => 'admin',
            ]
        );
    }
}
