<?php

use App\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::resource('add-item',ItemController::class);
Route::resource('borrow-item',BorrowController::class);
Route::resource('loan',LoanDataController::class);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profile', 'ProfileController@index')->name('profile');
Route::put('/profile', 'ProfileController@update')->name('profile.update');


Route::get('/add-item', 'ItemController@index')->name('add-item');
Route::get('/delete-item/{id}', 'ItemController@destroy')->name('delete-item');
Route::get('/edit-item/{id}', 'ItemController@edit')->name('edit-item');
Route::get('/loan-data', 'LoanDataController@index')->name('loan-data');

Route::get('/borrow-item', 'BorrowController@index')->name('borrow-item');
Route::get('/borrow-edit/{id}', 'BorrowController@edit')->name('borrow-edit');
Route::get('/borrow-delete/{delete}', 'BorrowController@destroy')->name('borrow-delete');

Route::get('/loan-export', 'LoanDataController@export')->name('loan-export');
